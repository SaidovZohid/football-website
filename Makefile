-include .env
.SILENT:
CURRENT_DIR=$(shell pwd)
DB_URL=postgresql://$(POSTGRES_USER):$(POSTGRES_PASSWORD)@$(POSTGRES_HOST):$(POSTGRES_PORT)/$(POSTGRES_DATABASE)?sslmode=disable

run:
	go run cmd/main.go

compose-up:
	docker compose up

migrate-up:
	migrate -path migrations -database "$(DB_URL)" -verbose up

migrate-up1:
	migrate -path migrations -database "$(DB_URL)" -verbose up 1

migrate-down:
	migrate -path migrations -database "$(DB_URL)" -verbose down

migrate-down1:
	migrate -path migrations -database "$(DB_URL)" -verbose down 1

create-migration:
	migrate create -ext sql -dir migrations -seq $(name)

unit-test:
	go test -v -cover ./storage/postgres/...

lint:
	golangci-lint run

cache:
	go clean -testcache

tidy:
	go mod tidy
	go mod vendor

swag-init:
	swag init -g api/api.go -o api/docs