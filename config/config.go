package config

import (
	"time"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

type Config struct {
	HttpPort                  string
	TokenSecretKey            string
	AuthorizationHeaderKey    string
	AuthorizationPayloadKey   string
	AccessTokenExpireDuration time.Duration
	Postgres                  PostgresConfig
}

type PostgresConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
}

func Load(path string) Config {
	godotenv.Load(path + "/.env")

	conf := viper.New()
	conf.AutomaticEnv()

	cfg := Config{
		HttpPort:                  conf.GetString("HTTP_PORT"),
		TokenSecretKey:            conf.GetString("TOKEN_SECRET_KEY"),
		AccessTokenExpireDuration: conf.GetDuration("ACCESS_TOKEN_DURATION"),
		AuthorizationHeaderKey:    conf.GetString("AUTHORIZATION_HEADER_KEY"),
		AuthorizationPayloadKey:   conf.GetString("AUTHORIZATION_PAYLOAD_KEY"),
		Postgres: PostgresConfig{
			Host:     conf.GetString("POSTGRES_HOST"),
			Port:     conf.GetString("POSTGRES_PORT"),
			User:     conf.GetString("POSTGRES_USER"),
			Password: conf.GetString("POSTGRES_PASSWORD"),
			Database: conf.GetString("POSTGRES_DATABASE"),
		},
	}
	return cfg
}
