CREATE TABLE IF NOT EXISTS "admins" (
    "id" uuid DEFAULT gen_random_uuid() PRIMARY KEY,
    "first_name" varchar NOT NULL,
    "last_name" varchar NOT NULL,
    "email" varchar NOT NULL,
    "password" varchar NOT NULL,
    "created_at" timestamp with time zone DEFAULT current_timestamp,
    "updated_at" timestamp,
    UNIQUE("email")
);

CREATE TABLE IF NOT EXISTS "posts" (
    "id" uuid DEFAULT gen_random_uuid() PRIMARY KEY,
    "title" varchar NOT NULL,
    "description" text NOT NULL,
    "image_url" varchar not null,
    "admin_id" uuid REFERENCES admins(id) ON DELETE CASCADE,
    "created_at" timestamp with time zone DEFAULT current_timestamp,
    UNIQUE("title")
);

CREATE TABLE IF NOT EXISTS "leagues" (
    "id" uuid DEFAULT gen_random_uuid() PRIMARY KEY,
    "name" varchar NOT NULL,
    "image_url" varchar NOT NULL,
    "created_at" timestamp with time zone DEFAULT current_timestamp,
    UNIQUE("name")
);

CREATE TABLE IF NOT EXISTS "clubs" (
    "id" uuid DEFAULT gen_random_uuid() PRIMARY KEY,
    "name" varchar NOT NULL,
    "image_url" varchar NOT NULL,
    "league_id" uuid REFERENCES leagues(id) ON DELETE CASCADE,
    "matches" int DEFAULT 0,
    "scores" int DEFAULT 0,
    "created_at" timestamp with time zone DEFAULT current_timestamp,
    UNIQUE("name")
);

CREATE TABLE IF NOT EXISTS "matches" (
    "id" uuid DEFAULT gen_random_uuid() PRIMARY KEY,
    "league_id" uuid REFERENCES leagues(id) ON DELETE CASCADE,
    "first_club_id" uuid REFERENCES clubs(id) ON DELETE CASCADE,
    "first_club_score" int DEFAULT 0 NOT NULL,
    "second_club_id" uuid REFERENCES clubs(id) ON DELETE CASCADE,
    "second_club_score" int DEFAULT 0 NOT NULL,
    "starts_at" timestamp NOT NULL, 
    "created_at" timestamp with time zone DEFAULT current_timestamp 
);