package models

import (
	"time"

	"github.com/google/uuid"
)

type CreateOrUpdateClub struct {
	Name     string    `json:"name" binding:"required"`
	ImageUrl string    `json:"image_url" binding:"required"`
	LeagueID uuid.UUID `json:"league_id" binding:"required"`
}

type ClubRes struct {
	ID        uuid.UUID `json:"id"`
	Name      string    `json:"name"`
	ImageUrl  string    `json:"image_url"`
	LeagueID  uuid.UUID `json:"league_id"`
	Matches   int64     `json:"matches"`
	Scores    int64     `json:"scores"`
	CreatedAt time.Time `json:"created_at"`
}
