package models

import (
	"time"

	"github.com/google/uuid"
)

type CreateOrUpdateLeague struct {
	Name     string `json:"name" binding:"required,min=2,max=30"`
	ImageUrl string `json:"image_url" binding:"required"`
}

type LeagueRes struct {
	ID        uuid.UUID `json:"id"`
	Name      string    `json:"name"`
	ImageUrl  string    `json:"image_url"`
	CreatedAt time.Time `json:"created_at"`
}

type GetAllLeaguesRes struct {
	Leagues []*LeagueRes `json:"leagues"`
	Count   int64        `json:"count"`
}
