package models

type LoginRequest struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required,min=6,max=12"`
}

type LoginResponse struct {
	AdminInfo   *AdminRes `json:"admin_info"`
	AccessToken string    `json:"access_token"`
	ExpiresAt   int64     `json:"expires_at"`
}
