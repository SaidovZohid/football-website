package models

import (
	"time"

	"github.com/google/uuid"
)

type PostRes struct {
	ID          uuid.UUID `json:"id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	ImageUrl    string    `json:"image_url"`
	AdminInfo   AdminRes  `json:"admin_info"`
	CreatedAt   time.Time `json:"created_at"`
}

type Post struct {
	ID          uuid.UUID `json:"id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	ImageUrl    string    `json:"image_url"`
	CreatedAt   time.Time `json:"created_at"`
}

type PostReq struct {
	Title       string `json:"title" binding:"required,min=5"`
	Description string `json:"description" binding:"required"`
	ImageUrl    string `json:"image_url" binding:"required"`
}

type GetAllPostsRes struct {
	Posts []*PostRes `json:"posts"`
	Count int64      `json:"count"`
}

type GetAllPostsParams struct {
	Limit  int64  `json:"limit" binding:"required" default:"10"`
	Page   int64  `json:"page" binding:"required" default:"1"`
	Search string `json:"search"`
}
