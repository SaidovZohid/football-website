package models

type FailureResponse struct {
	Message string `json:"message"`
	Error   string `json:"error"`
}

type ResponseOK struct {
	Message string `json:"message"`
}
