package v1

import (
	"errors"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"

	"github.com/SaidovZohid/football-website/api/models"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type File struct {
	File *multipart.FileHeader `form:"file" binding:"required"`
}

// @Security ApiKeyAuth
// @Summary File upload
// @Description File upload
// @Tags file-upload
// @Accept json
// @Produce json
// @Param file formData file true "File"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.FailureResponse
// @Router /file_upload [post]
func (h *handlerV1) UploadFile(ctx *gin.Context) {
	var file File

	if err := ctx.ShouldBind(&file); err != nil {
		h.HttpError(ctx, http.StatusBadRequest, "Error parsing request file. Please ensure it is a valid file", err)
		return
	}
	id := uuid.New()
	fileName := id.String() + filepath.Ext(file.File.Filename)
	dir, _ := os.Getwd()

	if _, err := os.Stat(dir + "/medias"); os.IsNotExist(err) {
		os.Mkdir(dir+"/medias", os.ModePerm)
	}

	filePath := "/medias/" + fileName
	err := ctx.SaveUploadedFile(file.File, dir+filePath)
	if err != nil {
		h.HttpError(ctx, http.StatusInternalServerError, "something went wrong", errors.New("internal server error"))
		return
	}
	ctx.JSON(http.StatusCreated, models.ResponseOK{
		Message: "https://zohiddev.uz" + filePath,
	})
}
