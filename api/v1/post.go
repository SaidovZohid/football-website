package v1

import (
	"database/sql"
	"errors"
	"net/http"

	"github.com/SaidovZohid/football-website/api/models"
	"github.com/SaidovZohid/football-website/storage/repo"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// Create post
// @Security ApiKeyAuth
// @Summary	Create post
// @Tags post
// @Description this api for creating post
// @Accept json
// @Produce json
// @Param data body models.PostReq true "Data"
// @Success 201 {object}  models.PostRes
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /posts [post]
func (h *handlerV1) CreatePost(ctx *gin.Context) {
	var req models.PostReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.HttpError(ctx, http.StatusBadRequest, "Error parsing request body. Please ensure it is a valid JSON and includes all required fields", err)
		return
	}
	claims, err := h.GetAuthPayload(ctx)
	if err != nil {
		h.HttpError(ctx, http.StatusBadRequest, "payload not found", err)
		return
	}

	adminId, ok := claims["id"]
	if !ok {
		h.HttpError(ctx, http.StatusInternalServerError, "something went wrong", errors.New("internal server error"))
		return
	}

	uuId, err := uuid.Parse(adminId.(string))
	if err != nil {
		h.HttpError(ctx, http.StatusInternalServerError, "something went wrong", errors.New("internal server error"))
		return
	}
	post, err := h.strg.Post().Create(ctx, &repo.Post{
		Title:       req.Title,
		Description: req.Description,
		ImageUrl:    req.ImageUrl,
		AdminID:     uuId,
	})
	if err != nil {
		h.HttpError(ctx, http.StatusInternalServerError, "internal server error", err)
		return
	}

	adminInfo, err := h.strg.Admin().Get(ctx, adminId.(string))
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.HttpError(ctx, http.StatusNotFound, "user with such an id does not exists", err)
			return
		}
		h.HttpError(ctx, http.StatusInternalServerError, "error while creating new adming, please try again", err)
		return
	}

	ctx.JSON(http.StatusCreated, models.PostRes{
		ID:          post.ID,
		Title:       post.Title,
		Description: post.Description,
		AdminInfo: models.AdminRes{
			ID:        adminInfo.ID,
			FirstName: adminInfo.FirstName,
			LastName:  adminInfo.LastName,
			Email:     adminInfo.Email,
			CreatedAt: adminInfo.CreatedAt,
			UpdatedAt: adminInfo.UpdatedAt,
		},
		CreatedAt: post.CreatedAt,
	})
}

// Get post
// @Summary	Get post
// @Tags post
// @Description this api for getting post
// @Produce json
// @Param id path string true "ID"
// @Succes 200 {object}  models.Post
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /posts/{id} [get]
func (h *handlerV1) GetPost(ctx *gin.Context) {
	post, err := h.strg.Post().Get(ctx, ctx.Param("id"))
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.HttpError(ctx, http.StatusNotFound, "post with such an id does not exists", err)
			return
		}
		h.HttpError(ctx, http.StatusInternalServerError, "error while getting post, please try again", err)
		return
	}

	ctx.JSON(http.StatusOK, models.Post{
		ID:          post.ID,
		Title:       post.Title,
		Description: post.Description,
		ImageUrl:    post.ImageUrl,
		CreatedAt:   post.CreatedAt,
	})
}

// Update post
// @Security ApiKeyAuth
// @Summary	Update post
// @Tags post
// @Description this api for updating post
// @Accept json
// @Produce json
// @Param data body models.PostReq true "Data"
// @Param id path string true "ID"
// @Success 200 {object}  models.Post
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /posts/{id} [put]
func (h *handlerV1) UpdatePost(ctx *gin.Context) {
	var req models.PostReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.HttpError(ctx, http.StatusBadRequest, "Error parsing request body. Please ensure it is a valid JSON and includes all required fields", err)
		return
	}
	uuID, err := uuid.Parse(ctx.Param("id"))
	if err != nil {
		h.HttpError(ctx, http.StatusBadRequest, "Error parsing request path", err)
		return
	}

	post, err := h.strg.Post().Update(ctx, &repo.Post{
		ID:          uuID,
		Title:       req.Title,
		Description: req.Description,
		ImageUrl:    req.ImageUrl,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.HttpError(ctx, http.StatusNotFound, "post with such an id does not exists", err)
			return
		}
		h.HttpError(ctx, http.StatusInternalServerError, "internal server error", err)
		return
	}

	ctx.JSON(http.StatusOK, models.Post{
		ID:          post.ID,
		Title:       post.Title,
		Description: post.Description,
		ImageUrl:    post.ImageUrl,
		CreatedAt:   post.CreatedAt,
	})
}

// Delete post
// @Security ApiKeyAuth
// @Summary	Delete post
// @Tags post
// @Description this api for deleting post
// @Produce json
// @Param id path string true "ID"
// @Success 200 {object}  models.PostRes
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /posts/{id} [delete]
func (h *handlerV1) DeletePost(ctx *gin.Context) {
	err := h.strg.Post().Delete(ctx, ctx.Param("id"))
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.HttpError(ctx, http.StatusNotFound, "post with such an id does not exists", err)
			return
		}
		h.HttpError(ctx, http.StatusInternalServerError, "error while getting post, please try again", err)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseOK{
		Message: "Succesfully deleted!",
	})
}

// GetAll posts
// @Summary	GetAll posts
// @Tags post
// @Description this api for getting posts
// @Produce json
// @Param params query models.GetAllPostsParams false "Params"
// @Success 200 {object}  models.GetAllPostsRes
// @Failure 400 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /posts [get]
func (h *handlerV1) GetAllPosts(ctx *gin.Context) {
	params, err := validateGetAllParams(ctx)
	if err != nil {
		h.HttpError(ctx, http.StatusBadRequest, "Error parsing request body. Please ensure it is a valid JSON and includes all required fields", err)
		return
	}

	res, err := h.strg.Post().GetAll(ctx, &repo.GetAllPostsParams{
		Limit:  params.Limit,
		Page:   params.Page,
		Search: params.Search,
	})
	if err != nil {
		h.HttpError(ctx, http.StatusInternalServerError, "something went wrong, try again!", err)
		return
	}

	response := models.GetAllPostsRes{
		Posts: make([]*models.PostRes, 0),
		Count: res.Count,
	}
	for _, v := range res.Posts {
		adminInfo, err := h.strg.Admin().Get(ctx, v.AdminID.String())
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				h.HttpError(ctx, http.StatusNotFound, "user with such an id does not exists", err)
				return
			}
			h.HttpError(ctx, http.StatusInternalServerError, "error while creating new adming, please try again", err)
			return
		}

		response.Posts = append(response.Posts, &models.PostRes{
			ID:          v.ID,
			Title:       v.Title,
			Description: v.Description,
			AdminInfo: models.AdminRes{
				ID:        adminInfo.ID,
				FirstName: adminInfo.FirstName,
				LastName:  adminInfo.LastName,
				Email:     adminInfo.Email,
				CreatedAt: adminInfo.CreatedAt,
				UpdatedAt: adminInfo.UpdatedAt,
			},
			CreatedAt: v.CreatedAt,
		})
	}

	ctx.JSON(http.StatusOK, response)
}
