package v1

import (
	"errors"
	"net/http"

	"github.com/SaidovZohid/football-website/api/models"
	"github.com/SaidovZohid/football-website/pkg/utils"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func (h *handlerV1) AuthMiddleWare(ctx *gin.Context) {
	accessToken := ctx.GetHeader(h.cfg.AuthorizationHeaderKey)

	if len(accessToken) == 0 {
		err := errors.New("authorization header is not provided")
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, models.FailureResponse{
			Message: "authorization header is not provided",
			Error:   err.Error(),
		})
		return
	}
	claims, err := utils.ExtractClaims(accessToken, h.cfg.TokenSecretKey)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, models.FailureResponse{
			Message: "authorization claims not provided",
			Error:   err.Error(),
		})
		return
	}

	ctx.Set(h.cfg.AuthorizationPayloadKey, claims)
	ctx.Next()
}

func (h *handlerV1) GetAuthPayload(ctx *gin.Context) (jwt.MapClaims, error) {
	i, exist := ctx.Get(h.cfg.AuthorizationPayloadKey)
	if !exist {
		return nil, errors.New("not found payload")
	}

	payload, ok := i.(jwt.MapClaims)
	if !ok {
		return nil, errors.New("unknown user")
	}
	return payload, nil
}
