package v1

import (
	"database/sql"
	"errors"
	"net/http"

	"github.com/SaidovZohid/football-website/api/models"
	"github.com/SaidovZohid/football-website/pkg/utils"
	"github.com/gin-gonic/gin"
)

// Login godoc
// @Summary Login user
// @Tags auth
// @Description login user, returns user data and token
// @Accept json
// @Produce json
// @Param data body models.LoginRequest true "data"
// @Success 200 {object} models.LoginResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /auth/login [post]
func (h *handlerV1) Login(ctx *gin.Context) {
	var req models.LoginRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.HttpError(ctx, http.StatusBadRequest, "Error parsing request body. Please ensure it is a valid JSON and includes all required fields", err)
		return
	}

	// getting user by email in order to check user exists
	user, err := h.strg.Admin().GetByEmail(ctx, req.Email)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.HttpError(ctx, http.StatusNotFound, "adming with the provided email does not exist", err)
			return
		}
		h.HttpError(ctx, http.StatusInternalServerError, "internal server error", err)
		return
	}

	// checking email and password
	if err := utils.CheckPassword(req.Password, user.Password); err != nil || user.Email != req.Email {
		h.HttpError(ctx, http.StatusUnauthorized, "Invalid email or password. Please try again", errors.New("invalid email or password. Please try again"))
		return
	}

	m := map[string]interface{}{
		"id":    user.ID.String(),
		"email": user.Email,
	}

	accessToken, expiresAt, err := utils.GenerateJWT(m, h.cfg.AccessTokenExpireDuration, h.cfg.TokenSecretKey)
	if err != nil {
		h.HttpError(ctx, http.StatusInternalServerError, "internal server error", err)
		return
	}

	response := models.LoginResponse{
		AdminInfo: &models.AdminRes{
			ID:        user.ID,
			FirstName: user.FirstName,
			LastName:  user.LastName,
			Email:     user.Email,
			CreatedAt: user.CreatedAt,
		},
		AccessToken: accessToken,
		ExpiresAt:   expiresAt,
	}

	ctx.JSON(http.StatusOK, response)
}

