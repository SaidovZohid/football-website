package v1

import (
	"database/sql"
	"errors"
	"net/http"

	"github.com/SaidovZohid/football-website/api/models"
	"github.com/SaidovZohid/football-website/pkg/utils"
	"github.com/SaidovZohid/football-website/storage/repo"
	"github.com/gin-gonic/gin"
)

// Create admin
// @Security ApiKeyAuth
// @Summary	Create admin
// @Tags admin
// @Description this api for creating admin
// @Accept json
// @Produce json
// @Param data body models.CreateAdminReq true "data"
// @Success 201 {object}  models.AdminRes
// @Failure 400 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /admins/create  [post]
func (h *handlerV1) CreateAdmin(ctx *gin.Context) {
	var req models.CreateAdminReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.HttpError(ctx, http.StatusBadRequest, "Error parsing request body. Please ensure it is a valid JSON and includes all required fields", err)
		return
	}

	if !isValidPassword(req.Password) {
		h.HttpError(ctx, http.StatusBadRequest, "Password should contain at least 6 and at most 12 characters, at least 1 number, at least 1 uppercase and 1 lowercase letter", errors.New("invalid password"))
		return
	}

	hashedPassword, err := utils.HashPassword(req.Password)
	if err != nil {
		h.HttpError(ctx, http.StatusInternalServerError, "internal server error", err)
		return
	}

	admin, err := h.strg.Admin().Create(ctx, &repo.Admin{
		FirstName: req.FirstName,
		LastName:  req.LastName,
		Email:     req.Email,
		Password:  hashedPassword,
	})
	if err != nil {
		h.HttpError(ctx, http.StatusInternalServerError, "error while creating new adming, please try again", err)
		return
	}

	ctx.JSON(http.StatusCreated, models.AdminRes{
		ID:        admin.ID,
		FirstName: admin.FirstName,
		LastName:  admin.LastName,
		Email:     admin.Email,
		CreatedAt: admin.CreatedAt,
		UpdatedAt: admin.UpdatedAt,
	})
}

// Get admin info
// @Security ApiKeyAuth
// @Summary	Get admin
// @Tags admin
// @Description this api for getting admin info
// @Produce json
// @Param id path string true "ID"
// @Success 200 {object}  models.AdminRes
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /admins/{id} [get]
func (h *handlerV1) GetAdminByID(ctx *gin.Context) {
	admin, err := h.strg.Admin().Get(ctx, ctx.Param("id"))
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.HttpError(ctx, http.StatusNotFound, "user with such an id does not exists", err)
			return
		}
		h.HttpError(ctx, http.StatusInternalServerError, "error while creating new adming, please try again", err)
		return
	}

	ctx.JSON(http.StatusCreated, models.AdminRes{
		ID:        admin.ID,
		FirstName: admin.FirstName,
		LastName:  admin.LastName,
		Email:     admin.Email,
		CreatedAt: admin.CreatedAt,
		UpdatedAt: admin.UpdatedAt,
	})
}
