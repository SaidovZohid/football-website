package v1

import (
	"strconv"

	"github.com/SaidovZohid/football-website/api/models"
	"github.com/SaidovZohid/football-website/config"
	"github.com/SaidovZohid/football-website/pkg/logger"
	"github.com/SaidovZohid/football-website/storage"
	"github.com/gin-gonic/gin"
)

type handlerV1 struct {
	cfg    *config.Config
	strg   storage.StorageI
	logger *logger.Logger
}

type HandlerV1Options struct {
	Cfg    *config.Config
	Strg   *storage.StorageI
	Logger *logger.Logger
}

func New(options *HandlerV1Options) *handlerV1 {
	return &handlerV1{
		cfg:    options.Cfg,
		strg:   *options.Strg,
		logger: options.Logger,
	}
}

func (h *handlerV1) HttpError(c *gin.Context, code int, message string, err error) {
	h.logger.Errorf("Error -> %v", err)
	c.JSON(code, models.FailureResponse{
		Message: message,
		Error:   err.Error(),
	})
}

func isValidPassword(password string) bool {
	var capitalLetter, smallLetter, number bool
	for i := 0; i < len(password); i++ {
		if password[i] >= 65 && password[i] <= 90 {
			capitalLetter = true
		} else if password[i] >= 97 && password[i] <= 122 {
			smallLetter = true
		} else if password[i] >= 48 && password[i] <= 57 {
			number = true
		}
	}
	return capitalLetter && smallLetter && number
}

func validateGetAllParams(ctx *gin.Context) (*models.GetAllPostsParams, error) {
	var (
		limit int64 = 10
		page  int64 = 1
		err   error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllPostsParams{
		Limit:  limit,
		Page:   page,
		Search: ctx.Query("search"),
	}, nil
}
