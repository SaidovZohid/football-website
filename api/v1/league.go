package v1

import (
	"database/sql"
	"errors"
	"net/http"

	"github.com/SaidovZohid/football-website/api/models"
	"github.com/SaidovZohid/football-website/storage/repo"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// Create league
// @Security ApiKeyAuth
// @Summary	Create league
// @Tags league
// @Description this api for creating league
// @Accept json
// @Produce json
// @Param data body models.CreateOrUpdateLeague true "Data"
// @Success 201  {object} models.LeagueRes
// @Failure 400 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /leagues [post]
func (h *handlerV1) CreateLeague(ctx *gin.Context) {
	var req models.CreateOrUpdateLeague
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.HttpError(ctx, http.StatusBadRequest, "Error parsing request body. Please ensure it is a valid JSON and includes all required fields", err)
		return
	}

	res, err := h.strg.League().Create(ctx, &repo.League{
		Name:     req.Name,
		ImageUrl: req.ImageUrl,
	})
	if err != nil {
		h.HttpError(ctx, http.StatusInternalServerError, "something went wrong", errors.New("internal server error"))
		return
	}

	ctx.JSON(http.StatusCreated, models.LeagueRes{
		ID:        res.ID,
		Name:      res.Name,
		ImageUrl:  res.ImageUrl,
		CreatedAt: res.CreatedAt,
	})
}

// Get league
// @Summary	Get league
// @Tags league
// @Description this api for getting league
// @Produce json
// @Param id path string true "ID"
// @Success  200 {object} models.LeagueRes
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /leagues/{id} [get]
func (h *handlerV1) GetLeague(ctx *gin.Context) {
	post, err := h.strg.League().Get(ctx, ctx.Param("id"))
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.HttpError(ctx, http.StatusNotFound, "league with such an id does not exists", err)
			return
		}
		h.HttpError(ctx, http.StatusInternalServerError, "error while getting league, please try again", err)
		return
	}

	// TODO: write getting all clubs which has in top in desc format

	ctx.JSON(http.StatusOK, models.LeagueRes{
		ID:        post.ID,
		Name:      post.Name,
		ImageUrl:  post.ImageUrl,
		CreatedAt: post.CreatedAt,
	})
}

// Update league
// @Summary	Update league
// @Security ApiKeyAuth
// @Tags league
// @Description this api for updating league
// @Accept json
// @Produce json
// @Param id path string true "ID"
// @Param data body models.CreateOrUpdateLeague true "Data"
// @Success  200 {object} models.LeagueRes
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /leagues/{id} [put]
func (h *handlerV1) UpdateLeague(ctx *gin.Context) {
	var req models.CreateOrUpdateLeague
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.HttpError(ctx, http.StatusBadRequest, "Error parsing request body. Please ensure it is a valid JSON and includes all required fields", err)
		return
	}

	uuID, err := uuid.Parse(ctx.Param("id"))
	if err != nil {
		h.HttpError(ctx, http.StatusBadRequest, "Error parsing request param. Please ensure it is given an id", err)
		return
	}

	res, err := h.strg.League().Update(ctx, &repo.League{
		ID:       uuID,
		Name:     req.Name,
		ImageUrl: req.ImageUrl,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.HttpError(ctx, http.StatusNotFound, "league with such an id does not exists", err)
			return
		}
		h.HttpError(ctx, http.StatusInternalServerError, "error while getting league, please try again", err)
		return
	}

	ctx.JSON(http.StatusOK, models.LeagueRes{
		ID:        res.ID,
		Name:      res.Name,
		ImageUrl:  res.ImageUrl,
		CreatedAt: res.CreatedAt,
	})
}

// Delete league
// @Summary	Delete league
// @Security ApiKeyAuth
// @Tags league
// @Description this api for deleting league
// @Produce json
// @Param id path string true "ID"
// @Succes  200 {object} models.ResponseOk
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /leagues/{id} [delete]
func (h *handlerV1) DeleteLeague(ctx *gin.Context) {
	err := h.strg.League().Delete(ctx, ctx.Param("id"))
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.HttpError(ctx, http.StatusNotFound, "league with such an id does not exists", err)
			return
		}
		h.HttpError(ctx, http.StatusInternalServerError, "error while getting league, please try again", err)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseOK{
		Message: "Successfully deleted!",
	})
}

// GetAll leagues
// @Summary	GetAll leagues
// @Tags league
// @Description this api for getting leagues
// @Produce json
// @Param params query models.GetAllPostsParams false "Params"
// @Success  200 {object} models.GetAllLeaguesRes
// @Failure 400 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /leagues [get]
func (h *handlerV1) GetAllLeagues(ctx *gin.Context) {
	params, err := validateGetAllParams(ctx)
	if err != nil {
		h.HttpError(ctx, http.StatusBadRequest, "Error parsing request body. Please ensure it is a valid JSON and includes all required fields", err)
		return
	}

	res, err := h.strg.League().GetAll(ctx, &repo.GetAllLeaguesParams{
		Limit:  params.Limit,
		Page:   params.Page,
		Search: params.Search,
	})
	if err != nil {
		h.HttpError(ctx, http.StatusInternalServerError, "something went wrong, try again!", err)
		return
	}

	response := models.GetAllLeaguesRes{
		Leagues: make([]*models.LeagueRes, 0),
		Count:   res.Count,
	}
	for _, v := range res.Leagues {
		response.Leagues = append(response.Leagues, &models.LeagueRes{
			ID:        v.ID,
			Name:      v.Name,
			ImageUrl:  v.ImageUrl,
			CreatedAt: v.CreatedAt,
		})
	}

	ctx.JSON(http.StatusOK, response)
}
