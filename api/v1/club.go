package v1

import (
	"errors"
	"net/http"

	"github.com/SaidovZohid/football-website/api/models"
	"github.com/SaidovZohid/football-website/storage/repo"
	"github.com/gin-gonic/gin"
)

// Create club
// @Security ApiKeyAuth
// @Summary	Create club
// @Tags club
// @Description this api for creating club
// @Accept json
// @Produce json
// @Param data body models.CreateOrUpdateClub true "Data"
// @Success 201  {object} models.ClubRes
// @Failure 400 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Router /clubs [post]
func (h *handlerV1) CreateClub(ctx *gin.Context) {
	var req models.CreateOrUpdateClub
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.HttpError(ctx, http.StatusBadRequest, "Error parsing request body. Please ensure it is a valid JSON and includes all required fields", err)
		return
	}

	club, err := h.strg.Club().Create(ctx, &repo.Club{
		Name:     req.Name,
		ImageUrl: req.ImageUrl,
		LeagueID: req.LeagueID,
	})
	if err != nil {
		h.HttpError(ctx, http.StatusInternalServerError, "something went wrong", errors.New("internal server error"))
		return
	}

	ctx.JSON(http.StatusCreated, models.ClubRes{
		ID:        club.ID,
		Name:      club.Name,
		ImageUrl:  club.ImageUrl,
		LeagueID:  club.LeagueID,
		Matches:   club.Matches,
		Scores:    club.Scores,
		CreatedAt: club.CreatedAt,
	})
}
