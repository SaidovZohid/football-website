package api

import (
	v1 "github.com/SaidovZohid/football-website/api/v1"
	"github.com/SaidovZohid/football-website/config"
	"github.com/SaidovZohid/football-website/pkg/logger"
	"github.com/SaidovZohid/football-website/storage"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/gin-contrib/cors"
)

type RoutetOptions struct {
	Cfg     *config.Config
	Storage storage.StorageI
	Logger  *logger.Logger
}

// New @title           Swagger for djinni endpoints
// @version         2.0
// @description     Djinni Backend Endpoints
// @BasePath  		/v1
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Security ApiKeyAuth
func New(opt *RoutetOptions) *gin.Engine {
	router := gin.Default()

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowCredentials = true
	corsConfig.AllowHeaders = append(corsConfig.AllowHeaders, "*")
	router.Use(cors.New(corsConfig))

	handlerV1 := v1.New(&v1.HandlerV1Options{
		Cfg:    opt.Cfg,
		Strg:   &opt.Storage,
		Logger: opt.Logger,
	})

	router.Static("/medias", "./medias")
	apiV1 := router.Group("/v1")

	apiV1.POST("/auth/login", handlerV1.Login)

	apiV1.POST("/file_upload", handlerV1.AuthMiddleWare, handlerV1.UploadFile)

	apiV1.POST("/admins/create", handlerV1.AuthMiddleWare, handlerV1.CreateAdmin)
	apiV1.GET("/admins/:id", handlerV1.AuthMiddleWare, handlerV1.GetAdminByID)

	apiV1.POST("/posts", handlerV1.AuthMiddleWare, handlerV1.CreatePost)
	apiV1.GET("/posts/:id", handlerV1.GetPost)
	apiV1.PUT("/posts/:id", handlerV1.AuthMiddleWare, handlerV1.UpdatePost)
	apiV1.DELETE("/posts/:id", handlerV1.AuthMiddleWare, handlerV1.DeletePost)
	apiV1.GET("/posts", handlerV1.GetAllPosts)

	apiV1.POST("/leagues", handlerV1.AuthMiddleWare, handlerV1.CreateLeague)
	apiV1.GET("/leagues/:id", handlerV1.GetLeague)
	apiV1.PUT("/leagues/:id", handlerV1.AuthMiddleWare, handlerV1.UpdateLeague)
	apiV1.DELETE("/leagues/:id", handlerV1.AuthMiddleWare, handlerV1.DeleteLeague)
	apiV1.GET("/leagues", handlerV1.GetAllLeagues)

	apiV1.POST("/clubs", handlerV1.AuthMiddleWare, handlerV1.CreateClub)

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router
}
