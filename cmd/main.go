package main

import (
	"fmt"
	"log"

	"github.com/SaidovZohid/football-website/api"
	_ "github.com/SaidovZohid/football-website/api/docs"
	"github.com/SaidovZohid/football-website/config"
	"github.com/SaidovZohid/football-website/pkg/logger"
	"github.com/SaidovZohid/football-website/storage"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func main() {
	cfg := config.Load(".")

	logger.Init()
	logger := logger.GetLogger()

	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)

	if err != nil {
		log.Fatalf("failed to connect to database: %v", err)
	}

	strg := storage.NewStoragePg(psqlConn)

	apiServer := api.New(&api.RoutetOptions{
		Cfg:     &cfg,
		Logger:  &logger,
		Storage: strg,
	})

	log.Println("server started")
	err = apiServer.Run(cfg.HttpPort)
	if err != nil {
		log.Fatalf("failed to run server: %v", err)
	}
}
