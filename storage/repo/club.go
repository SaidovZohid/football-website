package repo

import (
	"context"
	"time"

	"github.com/google/uuid"
)

type ClubStorageI interface {
	Create(ctx context.Context, club *Club) (*Club, error)
	GetAll(ctx context.Context, params *GetAllClubsParams) (*GetAllClubs, error)
}

type Club struct {
	ID        uuid.UUID
	Name      string
	ImageUrl  string
	LeagueID  uuid.UUID
	Matches   int64
	Scores    int64
	CreatedAt time.Time
}

type GetAllClubs struct {
	Clubs []*Club
	Count int64
}

type GetAllClubsParams struct {
	Limit    int64
	Page     int64
	Search   string
	LeagueID string
}
