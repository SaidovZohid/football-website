package repo

import (
	"context"
	"time"

	"github.com/google/uuid"
)

type PostStorageI interface {
	Create(ctx context.Context, post *Post) (*Post, error)
	Get(ctx context.Context, id string) (*Post, error)
	Update(ctx context.Context, post *Post) (*Post, error)
	Delete(ctx context.Context, id string) error
	GetAll(ctx context.Context, params *GetAllPostsParams)  (*GetAllPosts, error)
}

type Post struct {
	ID          uuid.UUID
	Title       string
	Description string
	ImageUrl    string
	AdminID     uuid.UUID
	CreatedAt   time.Time
}

type GetAllPosts struct {
	Posts []*Post
	Count int64
}

type GetAllPostsParams struct {
	Limit  int64
	Page   int64
	Search string // by title
}
