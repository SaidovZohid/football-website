package repo

import (
	"context"
	"time"

	"github.com/google/uuid"
)

type AdminStorageI interface {
	Create(ctx context.Context, admin *Admin) (*Admin, error)
	Get(ctx context.Context, adminId string) (*Admin, error)
	GetByEmail(ctx context.Context, email string) (*Admin, error)
}

type Admin struct {
	ID        uuid.UUID
	FirstName string
	LastName  string
	Email     string
	Password  string
	CreatedAt time.Time
	UpdatedAt *time.Time
}
