package repo

import (
	"context"
	"time"

	"github.com/google/uuid"
)

type LeagueStorageI interface {
	Create(ctx context.Context, league *League) (*League, error)
	Get(ctx context.Context, id string) (*League, error)
	Update(ctx context.Context, league *League) (*League, error)
	Delete(ctx context.Context, id string) error
	GetAll(ctx context.Context, params *GetAllLeaguesParams) (*GetAllLeagues, error)
}

type League struct {
	ID        uuid.UUID
	Name      string
	ImageUrl  string
	CreatedAt time.Time
}

type GetAllLeagues struct {
	Leagues []*League
	Count   int64
}

type GetAllLeaguesParams struct {
	Limit  int64
	Page   int64
	Search string
}
