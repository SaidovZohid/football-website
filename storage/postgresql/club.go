package postgresql

import (
	"context"
	"fmt"

	"github.com/SaidovZohid/football-website/storage/repo"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type clubRepo struct {
	db *sqlx.DB
}

func NewClub(db *sqlx.DB) repo.ClubStorageI {
	return &clubRepo{
		db: db,
	}
}

func (c *clubRepo) Create(ctx context.Context, club *repo.Club) (*repo.Club, error) {
	tx, err := c.db.Begin()
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()
	query := `
		INSERT INTO clubs(name, image_url, league_id, matches, scores) VALUES ($1,$2,$3,$4,$5) RETURNING id, created_at
	`
	err = tx.QueryRow(query, club.Name, club.ImageUrl, club.LeagueID, club.Matches, club.Scores).Scan(&club.ID, &club.CreatedAt)

	return club, err
}

func (c *clubRepo) GetAll(ctx context.Context, params *repo.GetAllClubsParams) (*repo.GetAllClubs, error) {
	// TODO: write get all clubs sql query
	offset := (params.Page - 1) * params.Limit
	// res := repo.GetAllClubs{
	// 	Clubs: make([]*repo.Club, 0),
	// }

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := " WHERE true "
	if params.Search != "" {
		filter += fmt.Sprintf(`
			AND name ILIKE '%s'
		`, "%"+params.Search+"%")
	}
	if params.LeagueID != "" {
		uuID, err := uuid.Parse(params.LeagueID)
		if err != nil {
			return nil, err
		}
		filter += fmt.Sprintf(`
			AND league_id = %v
		`, uuID)
	}

	query := `
	SELECT 
		id, name, image_url, league_id, matches, scores, created_at
	FROM leagues 
	` + filter + `ORDER BY matches, scores ASC ` + limit

	_ = query

	return nil, nil
}
