package postgresql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/SaidovZohid/football-website/storage/repo"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type postRepo struct {
	db *sqlx.DB
}

func NewPost(db *sqlx.DB) repo.PostStorageI {
	return &postRepo{
		db: db,
	}
}

func (p *postRepo) Create(ctx context.Context, post *repo.Post) (*repo.Post, error) {
	tx, err := p.db.Begin()
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()
	query := `
		INSERT INTO posts(title, description, image_url, admin_id) VALUES ($1, $2, $3, $4)
		RETURNING id, created_at
	`
	err = tx.QueryRow(query, post.Title, post.Description, post.ImageUrl, post.AdminID).Scan(&post.ID, &post.CreatedAt)
	if err != nil {
		return nil, err
	}

	return post, nil
}

func (p *postRepo) Get(ctx context.Context, id string) (*repo.Post, error) {
	uuId, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}
	qeury := `
		SELECT id, title, description, image_url, admin_id, created_at
		FROM posts WHERE id = $1
	`
	var post repo.Post
	err = p.db.QueryRow(qeury, uuId).Scan(&post.ID, &post.Title, &post.Description, &post.ImageUrl, &post.AdminID, &post.CreatedAt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, sql.ErrNoRows
		}
		return nil, err
	}

	return &post, nil
}

func (p *postRepo) Update(ctx context.Context, post *repo.Post) (*repo.Post, error) {
	tx, err := p.db.Begin()
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()
	qeury := `
		UPDATE posts SET 
			title = $1, description = $2, image_url = $3 
		WHERE id = $4
	`
	res, err := tx.Exec(qeury, post.Title, post.Description, post.ImageUrl, post.ID)
	if err != nil {
		return nil, err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return nil, sql.ErrNoRows
	}

	return post, nil
}

func (p *postRepo) Delete(ctx context.Context, id string) error {
	tx, err := p.db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()
	uuId, err := uuid.Parse(id)
	if err != nil {
		return err
	}

	qeury := `
		DELETE FROM posts WHERE id = $1 
	`
	res, err := tx.Exec(qeury, uuId)
	if err != nil {
		return err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (p *postRepo) GetAll(ctx context.Context, params *repo.GetAllPostsParams) (*repo.GetAllPosts, error) {
	offset := (params.Page - 1) * params.Limit
	res := repo.GetAllPosts{
		Posts: make([]*repo.Post, 0),
	}

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := " WHERE true "
	if params.Search != "" {
		filter += fmt.Sprintf(`
			AND title ILIKE '%s'
		`, "%"+params.Search+"%")
	}

	query := `
	SELECT 
		id, title, description, image_url, admin_id, created_at
	FROM posts 
	` + filter + `ORDER BY created_at DESC ` + limit

	rows, err := p.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var post repo.Post
		err := rows.Scan(
			&post.ID,
			&post.Title,
			&post.Description,
			&post.ImageUrl,
			&post.AdminID,
			&post.CreatedAt,
		)
		if err != nil {
			return nil, err
		}
		res.Posts = append(res.Posts, &post)
	}

	queryCount := "SELECT count(1) FROM posts " + filter

	if err = p.db.QueryRow(queryCount).Scan(&res.Count); err != nil {
		return nil, err
	}

	return &res, nil
}
