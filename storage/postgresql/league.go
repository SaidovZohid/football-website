package postgresql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/SaidovZohid/football-website/storage/repo"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type leagueRepo struct {
	db *sqlx.DB
}

func NewLeague(db *sqlx.DB) repo.LeagueStorageI {
	return &leagueRepo{
		db: db,
	}
}

func (l *leagueRepo) Create(ctx context.Context, league *repo.League) (*repo.League, error) {
	tx, err := l.db.Begin()
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	query := `
		INSERT INTO leagues(name, image_url) VALUES ($1, $2) RETURNING id, created_at
	`
	err = tx.QueryRow(query, league.Name, league.ImageUrl).Scan(&league.ID, &league.CreatedAt)

	return league, err
}

func (l *leagueRepo) Get(ctx context.Context, id string) (*repo.League, error) {
	var res repo.League
	uuID, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT id, name, image_url, created_at FROM leagues WHERE id=$1
	`
	err = l.db.QueryRow(query, uuID).Scan(&res.ID, &res.Name, &res.ImageUrl, &res.CreatedAt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, sql.ErrNoRows
		}
		return nil, err
	}
	return &res, nil
}

func (l *leagueRepo) Update(ctx context.Context, league *repo.League) (*repo.League, error) {
	tx, err := l.db.Begin()
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	query := `
		UPDATE leagues SET name = $1, image_url = $2 WHERE id = $3 RETURNING created_at
	`
	err = tx.QueryRow(query, league.Name, league.ImageUrl, league.ID).Scan(&league.CreatedAt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, sql.ErrNoRows
		}
		return nil, err
	}

	return league, nil
}

func (l *leagueRepo) Delete(ctx context.Context, id string) error {
	tx, err := l.db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()
	uuID, err := uuid.Parse(id)
	if err != nil {
		return err
	}

	query := `
		DELETE FROM leagues WHERE id = $1
	`
	res, err := tx.Exec(query, uuID)
	if err != nil {
		return err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (l *leagueRepo) GetAll(ctx context.Context, params *repo.GetAllLeaguesParams) (*repo.GetAllLeagues, error) {
	offset := (params.Page - 1) * params.Limit
	res := repo.GetAllLeagues{
		Leagues: make([]*repo.League, 0),
	}

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := " WHERE true "
	if params.Search != "" {
		filter += fmt.Sprintf(`
			AND name ILIKE '%s'
		`, "%"+params.Search+"%")
	}

	query := `
	SELECT 
		id, name, image_url, created_at
	FROM leagues 
	` + filter + `ORDER BY created_at DESC ` + limit

	rows, err := l.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var league repo.League
		err := rows.Scan(
			&league.ID,
			&league.Name,
			&league.ImageUrl,
			&league.CreatedAt,
		)
		if err != nil {
			return nil, err
		}
		res.Leagues = append(res.Leagues, &league)
	}

	queryCount := "SELECT count(1) FROM leagues " + filter

	if err = l.db.QueryRow(queryCount).Scan(&res.Count); err != nil {
		return nil, err
	}

	return &res, nil
}
