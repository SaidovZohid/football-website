package postgresql

import (
	"context"
	"database/sql"
	"errors"

	"github.com/SaidovZohid/football-website/storage/repo"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type adminRepo struct {
	db *sqlx.DB
}

func NewAdmin(db *sqlx.DB) repo.AdminStorageI {
	return &adminRepo{
		db: db,
	}
}

func (a *adminRepo) Create(ctx context.Context, admin *repo.Admin) (*repo.Admin, error) {
	tx, err := a.db.Begin()
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()
	query := `
		INSERT INTO admins (first_name, last_name, email, password) VALUES ($1, $2, $3, $4)
		RETURNING id, created_at
	`
	err = tx.QueryRow(query, admin.FirstName, admin.LastName, admin.Email, admin.Password).Scan(&admin.ID, &admin.CreatedAt)

	return admin, err
}

func (a *adminRepo) Get(ctx context.Context, id string) (*repo.Admin, error) {
	uuId, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}
	query := `
		SELECT id, first_name, last_name, email, created_at, updated_at FROM admins WHERE id = $1
	`
	var res repo.Admin
	err = a.db.QueryRow(query, uuId).Scan(&res.ID, &res.FirstName, &res.LastName, &res.Email, &res.CreatedAt, &res.UpdatedAt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, sql.ErrNoRows
		}
		return nil, err
	}

	return &res, nil
}

func (a *adminRepo) GetByEmail(ctx context.Context, email string) (*repo.Admin, error) {
	query := `
		SELECT id, first_name, last_name, email, password, created_at, updated_at FROM admins WHERE email = $1
	`
	var res repo.Admin
	err := a.db.QueryRow(query, email).Scan(&res.ID, &res.FirstName, &res.LastName, &res.Email, &res.Password, &res.CreatedAt, &res.UpdatedAt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, sql.ErrNoRows
		}
		return nil, err
	}

	return &res, nil
}
