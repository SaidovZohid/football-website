package storage

import (
	"github.com/SaidovZohid/football-website/storage/postgresql"
	"github.com/SaidovZohid/football-website/storage/repo"
	"github.com/jmoiron/sqlx"
)

type StorageI interface {
	Admin() repo.AdminStorageI
	Post() repo.PostStorageI
	League() repo.LeagueStorageI
	Club() repo.ClubStorageI
}

type StoragePg struct {
	adminRepo  repo.AdminStorageI
	postRepo   repo.PostStorageI
	leagueRepo repo.LeagueStorageI
	clubRepo   repo.ClubStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &StoragePg{
		adminRepo:  postgresql.NewAdmin(db),
		postRepo:   postgresql.NewPost(db),
		leagueRepo: postgresql.NewLeague(db),
		clubRepo:   postgresql.NewClub(db),
	}
}

func (s *StoragePg) Admin() repo.AdminStorageI {
	return s.adminRepo
}

func (s *StoragePg) Post() repo.PostStorageI {
	return s.postRepo
}

func (s *StoragePg) League() repo.LeagueStorageI {
	return s.leagueRepo
}

func (s *StoragePg) Club() repo.ClubStorageI {
	return s.clubRepo
}
